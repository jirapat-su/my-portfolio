/** @type {import("eslint").Linter.Config} */

const config = {
  env: {
    browser: true,
    es2020: true,
    node: true,
  },
  extends: [
    'next/core-web-vitals',
    'eslint:recommended',
    'plugin:prettier/recommended',
    'plugin:@typescript-eslint/recommended-type-checked',
    'plugin:@typescript-eslint/recommended',
    'plugin:import/errors',
    'plugin:import/warnings',
    'plugin:import/typescript',
  ],
  ignorePatterns: ['node_modules', '.next', 'public', '.vscode'],
  overrides: [
    {
      env: {
        jest: true,
      },
      files: ['**/*.test.ts', '**/*.test.tsx'],
      rules: {
        '@typescript-eslint/no-unused-vars': 'off',
      },
    },
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 2020,
    project: './tsconfig.json',
    sourceType: 'module',
  },
  plugins: ['prettier', '@typescript-eslint', 'perfectionist', 'import'],
  root: true,
  rules: {
    '@typescript-eslint/no-misused-promises': [
      'error',
      {
        checksVoidReturn: false,
      },
    ],
    '@typescript-eslint/no-unused-vars': [
      'warn',
      {
        argsIgnorePattern: '^_',
        caughtErrors: 'all',
      },
    ],
    '@typescript-eslint/unbound-method': 'off',
    eqeqeq: ['error', 'always'],
    'import/default': 'error',
    'import/named': 'error',
    'import/namespace': 'error',
    'import/no-absolute-path': 'error',
    'import/no-unresolved': 'error',
    indent: [
      'error',
      2,
      {
        ArrayExpression: 1,
        CallExpression: { arguments: 'first' },
        FunctionExpression: { parameters: 'first' },
        ImportDeclaration: 1,
        MemberExpression: 1,
        ObjectExpression: 'first',
        StaticBlock: { body: 2 },
        SwitchCase: 1,
        VariableDeclarator: 'first',
        offsetTernaryExpressions: true,
        outerIIFEBody: 'off',
      },
    ],
    'react/no-unescaped-entities': 'off',
    'no-console': 'error',
    'no-eval': 'error',
    'no-var': 'error',
    'perfectionist/sort-objects': [
      'error',
      {
        order: 'asc',
        type: 'natural',
      },
    ],
    'prefer-const': 'error',
    'prettier/prettier': [
      'warn',
      {},
      {
        configFile: '.prettierrc.mjs',
        usePrettierrc: true,
      },
    ],
  },
  settings: {
    react: {
      version: 'detect',
    },
  },
};

module.exports = config;
