/** @type {import('prettier').Config & import('prettier-plugin-tailwindcss').options} */

const config = {
  arrowParens: 'always',
  bracketSameLine: false,
  bracketSpacing: true,
  endOfLine: 'lf',
  htmlWhitespaceSensitivity: 'css',
  jsxSingleQuote: true,
  plugins: ['prettier-plugin-organize-imports', 'prettier-plugin-tailwindcss'],
  printWidth: 150,
  semi: true,
  singleAttributePerLine: true,
  singleQuote: true,
  tabWidth: 2,
  tailwindConfig: './tailwind.config.ts',
  trailingComma: 'all',
  useTabs: false,
};

export default config;
