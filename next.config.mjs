/** @type {import('next').NextConfig} */

const nextConfig = {
  images: {
    minimumCacheTTL: 60,
    unoptimized: true,
  },
  output: process.env.BUILD_STANDALONE === 'true' ? 'standalone' : undefined,
  reactStrictMode: true,
};

export default nextConfig;
