import clsx from 'clsx';
import type { Metadata, Viewport } from 'next';
import { Prompt } from 'next/font/google';
import './globals.css';

const prompt = Prompt({ subsets: ['latin'], weight: ['100', '200', '300', '400', '500', '600', '700', '800', '900'] });

export const metadata: Metadata = {
  alternates: {
    canonical: new URL('https://jirapat-suk.web.app/'),
  },
  authors: [{ name: 'Jirapat Sukhonthapong', url: 'https://jirapat-suk.web.app/' }],
  category: 'Portfolio, Front-End Development, Web Development',
  creator: 'Jirapat Sukhonthapong',
  description:
    'Explore the portfolio website of Jirapat Sukhonthapong, a passionate Front-End Developer showcasing expertise in web development, user interfaces, and innovative digital experiences.',
  keywords: 'Jirapat Sukhonthapong, Front-End Developer, Portfolio, Web Development, User Interfaces, Digital Experiences',
  metadataBase: new URL('https://jirapat-suk.web.app/'),
  publisher: 'Jirapat Sukhonthapong',
  referrer: 'origin-when-cross-origin',
  robots: {
    follow: true,
    googleBot: {
      follow: true,
      index: true,
      'max-image-preview': 'large',
      'max-snippet': -1,
      'max-video-preview': -1,
      noimageindex: false,
    },
    index: true,
    nocache: true,
  },
  title: {
    absolute: 'Jirapat Sukhonthapong | Portfolio',
    default: 'Jirapat Sukhonthapong | Portfolio',
    template: '%s | Jirapat Sukhonthapong - Portfolio',
  },
  verification: {
    google: '9H9LKBK2z7_9px61BtE87mVwfuzfcuW2h97ZXk1gNY8',
    other: {
      me: ['https://jirapat-suk.web.app/'],
    },
    yahoo: 'yahoo',
    yandex: 'yandex',
  },
};

export const viewport: Viewport = {
  initialScale: 1,
  maximumScale: 2,
  minimumScale: 1,
  themeColor: [
    { color: '#121212', media: '(prefers-color-scheme: dark)' },
    { color: '#ffffff', media: '(prefers-color-scheme: light)' },
  ],
  width: 'device-width',
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang='en'>
      <body className={clsx(prompt.className, 'container mx-auto min-h-dvh bg-[#121212] px-8 py-8')}>{children}</body>
    </html>
  );
}
