import HeroSection from '@/components/hero-section';
import { Fragment } from 'react';

const Home = () => {
  return (
    <Fragment>
      <main>
        <HeroSection />
      </main>
    </Fragment>
  );
};

export default Home;
