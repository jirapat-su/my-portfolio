'use client';

import { motion } from 'framer-motion';
import Image from 'next/image';
import { FaDiscord, FaGitlab, FaLine, FaLinkedin } from 'react-icons/fa6';
import { TypeAnimation } from 'react-type-animation';

const workAge = new Date().getFullYear() - 2019;

const HeroSection = () => {
  return (
    <motion.section
      className='grid grid-cols-1 lg:grid-cols-12'
      initial={{ opacity: 0, scale: 0.5, y: 500 }}
      animate={{ opacity: 1, scale: 1, y: 0 }}
      transition={{ duration: 0.5, ease: 'easeOut' }}
    >
      <div className='col-span-7'>
        <div className='flex h-[100%] flex-col justify-center'>
          <h1 className='text-2xl font-extrabold text-white lg:text-4xl xl:text-6xl'>
            <span className='bg-gradient-to-r from-cyan-200 to-sky-500 bg-clip-text text-transparent'>Hello, I'm</span>
            <br />
            <TypeAnimation
              sequence={['Jirapat Sukhonthapong', 1000, 'Front-End Developer', 2000]}
              wrapper='span'
              speed={50}
              style={{ display: 'inline-block' }}
              repeat={Infinity}
              deletionSpeed={80}
            />
          </h1>

          <p className='mt-8 text-lg text-[#adb7be] lg:text-xl'>
            Hello, I'm Jirapat Sukhonthapong, a seasoned Software Developer with over {workAge} years of experience, based in Thailand. I have a
            passion for web development and love to create websites and web applications that are fast, responsive, and user-friendly.
          </p>

          <div className='mt-8 flex justify-center gap-8 text-4xl text-white lg:justify-start'>
            <motion.a
              whileHover={{ color: '#f05032', scale: 1.4 }}
              whileTap={{ scale: 0.9 }}
              href='https://gitlab.com/jirapat-su'
              target='_blank'
              aria-label='GitLab'
              className='rounded-lg'
            >
              <FaGitlab />
            </motion.a>

            <motion.a
              whileHover={{ backgroundColor: '#fff', color: '#0a66c2', scale: 1.4 }}
              whileTap={{ scale: 0.9 }}
              href='https://www.linkedin.com/in/jirapat-su'
              target='_blank'
              aria-label='LinkedIn'
              className='rounded-lg'
            >
              <FaLinkedin />
            </motion.a>

            <motion.a
              whileHover={{ backgroundColor: '#7289da', color: '#fff', scale: 1.4 }}
              whileTap={{ scale: 0.9 }}
              href='https://discord.com/users/909832962869194762'
              target='_blank'
              aria-label='Discord'
              className='rounded-lg'
            >
              <FaDiscord />
            </motion.a>

            <motion.a
              whileHover={{ backgroundColor: '#fff', color: '#00c300', scale: 1.4 }}
              whileTap={{ scale: 0.9 }}
              href='https://line.me/ti/p/hu_wphkPjU'
              target='_blank'
              aria-label='Line'
              className='rounded-lg'
            >
              <FaLine />
            </motion.a>
          </div>
        </div>
      </div>

      <div className='col-span-5'>
        <div className='flex items-center justify-center p-8'>
          <motion.div
            animate={{
              y: [0, 10, 0],
            }}
            transition={{ duration: 4, repeat: Infinity }}
            className='relative aspect-[1/1] w-[100%] max-w-[380px] rounded-full shadow-[0_0_20px_8px_#0ea5e9]'
          >
            <Image
              src='/images/avatar.webp'
              alt='hero image'
              className='rounded-full'
              priority
              unoptimized
              fill
            />
          </motion.div>
        </div>
      </div>
    </motion.section>
  );
};

export default HeroSection;
